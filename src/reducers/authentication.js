import {USER_VERIFY} from '../constants/ActionTypes'

export const initialState = {
    authenticated: false,
    msg: ''
};
const authHandler = (state = initialState, action) => {
    switch (action.type) {
        case USER_VERIFY:
            state.authenticated = action.authenticated
            state.msg = action.msg
            return {
                ...state
            }
        default:
            return {
                ...state
            }
    }
}
export default authHandler
