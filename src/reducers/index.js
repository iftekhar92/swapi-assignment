import { combineReducers } from 'redux';
import loginHandler from './login'
import authHandler from './authentication'

export default combineReducers({
    loginHandler,
    authHandler
})
