import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store';
import App from './components/app';
import 'babel-polyfill';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const store = configureStore();

ReactDOM.render((
    <MuiThemeProvider muiTheme={getMuiTheme()}>
      <App store={store} />
    </MuiThemeProvider>),document.getElementById('my-app')
);

module.hot.accept()
