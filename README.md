
### How to run
Run following commands
<pre><code>
<ol>
<li>Install node latest version (version >= 8)</li>
<li>npm install</li>
<li>npm start</li>
</ol>
</code></pre>

### Active URL
http://localhost:3700

Test from any user as mention in assignment. like:
Username: Luke Skywalker
Password : 19BBY